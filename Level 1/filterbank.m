%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function frameF = filterbank(frameT, frameType, winType)
%A function that transforms our samples from time domain to frequency
%domain using the mdct function after multiplying them with a kaizer bessel
%derived or sinusoid window. Because I use Matlab R2018a, the mdct function
%is not implemented, so I am using the function mdct4 created from Marios
%Athinaiou (http://www.ee.columbia.edu/~marios/)
%INPUT
%frameT: 2048x2 matrix with the samples of the two channels
%frameType: string containing the type of the current frame
%winType: string containing the type of window to be used, 'KDB' for kaiser
%bessel derived or 'SIN' for sinusoid
%OUTPUT
%frameF: 1024x2 matrix with the samples in the frequency domain

N=length(frameT(:,1));
winFrame=zeros(N,1);
frameF=zeros(N/2,2);
%%
%OLS
if(frameType=='OLS')
    %Create the windows
    if(winType=='KBD')
        w = kaiser(N/2+1,6*pi);
        for n=1:(N/2)
            winFrame(n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        for n=(N/2+1):N
            winFrame(n)=sqrt(sum(w(1:N+1-n))/sum(w));
        end
    elseif(winType=='SIN')
        for n=1:N
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
    end
    %Multiply the samples with the window created
    newFrameT=frameT.*winFrame;
    %Make the mdct transform
    frameF=mdct4(newFrameT);

end
%%
%LSS
if(frameType=='LSS')
    %Create the window
    if(winType=='KBD')
        w = kaiser(N/2+1,6*pi);
        for n=1:(N/2)
            winFrame(n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        %Here the window has only ones
        winFrame(((N/2)+1):((N/2)+1)+448-1)=ones(448,1);
        w=kaiser(129,6*pi);
        for n=129:256
            l=128-(256-n);
            winFrame(((N/2)+1)+448-1+l)=sqrt(sum(w(1:256+1-n))/sum(w));
        end
        %In the end it ends with zeros
        winFrame(((N/2)+1)+448+128:N)=zeros(448,1);
    elseif(winType=='SIN')
        for n=1:(N/2)
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
        winFrame(((N/2)+1):((N/2)+1)+448-1)=ones(448,1);
        for n=129:256
            l=128-(256-n);
            winFrame(((N/2)+1)+448-1+l)=sin((pi/(256))*((n-1)+1/2));
        end
        winFrame(((N/2)+1)+448+128:N)=zeros(448,1);
    end
    %Multiply the samples with the window created
    newFrameT=frameT.*winFrame;
    %Make the mdct transform
    frameF=mdct4(newFrameT);
    
end
%%  
%LPS    
if(frameType=='LPS')
    %Create the window
    if(winType=='KBD')
        %It begins with zeros
        winFrame(1:448)=zeros(448,1);
        w=kaiser(129,6*pi);
        for n=1:128
            winFrame(448+n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        %Here it has ones
        winFrame((448+128+1):(448+128+448))=ones(448,1);
        w = kaiser(N/2+1,6*pi);
        for n=(N/2+1):N
            winFrame(n)=sqrt(sum(w(1:N+1-n))/sum(w));
        end
    elseif(winType=='SIN')
        winFrame(1:448)=zeros(448,1);
        for n=1:128
            winFrame(448+n)=sin((pi/(256))*((n-1)+1/2));
        end
        winFrame((448+128+1):(448+128+448))=ones(448,1);
        for n=(N/2+1):N
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
    end
    %Multiply the samples with the window created  
    newFrameT=frameT.*winFrame;
    %Make the mdct transform
    frameF=mdct4(newFrameT);
end
%%
%ESH
if(frameType=='ESH')
    %Create the window for each of the subframes
    winFrame=zeros(256,1);
    N=256;
    if(winType=='KBD')
        w=kaiser(N/2+1,6*pi);
        for n=1:(N/2)
            winFrame(n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        for n=(N/2+1):N
            winFrame(n)=sqrt(sum(w(1:N+1-n))/sum(w));
        end
    elseif(winType=='SIN')
        for n=1:N
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
    end
    for i=0:7
        %Takes only the samples from one of each of the subframes
        subFrame=frameT(448+1+i*128:448+(i+2)*128,:);
        %Multiplies the samples with the window
        newSubFrameT=subFrame.*winFrame;
        %It creates the 1024x2 matrix from the 8x128x2 subframes in the
        %frequency domain created
        frameF(i*128+1:(i+1)*128,:)=mdct4(newSubFrameT);
    end            
    
end

end