%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function frameType = SSC(frameT, nextFrameT, prevFrameType)
%Function that decides what type of frame is the current frameT given the
%next Frame samples and previous frame type. The next frame samples are
%given only to check if it is Eight short so we can decide if the current
%will be ESH or Long start. FrameT parameter not needed for the function
%but it is told by the project to have it as one
%INPUT
%nextFrameT: 2048x2 matrix with the samples of the two channels
%prevFrameType: string containing the previous frame type as 'ESH', 'OLS',
%'LSS', 'LPS'
%OUTPUT
%frameType: string containing the current frame type declared same as
%prevFrameType

%%
%Filtering
b=[0.7548 -0.7548];
a=[1 -0.5095];
hNext=filter(b,a,nextFrameT(577:1600,:)); %Filter the data
nextFrameType="OLS"; %Assuming the next frame is Only Long. It is used only for initializing this variable
                     %We care only if the next is Eight short, which we
                     %check in the next part of the function. 

%%
%Check if next is ESH
for l=0:7
    sample=hNext(128*l+1:128*l+128,:);
    sl2right(1+l)=sum(sample(:,1).^2);
    sl2left(1+l)=sum(sample(:,2).^2);
    if(l==0)
        continue;
    end
    dsl2right=(l)*(sl2right(l+1))/sum(sl2right(1:l));
    dsl2left=(l)*(sl2left(l+1))/sum(sl2left(1:l));
    if((sl2right(1+l)>10^-3 && dsl2right>10) || (sl2left(1+l)>10^-3 && dsl2left>10))
        nextFrameType="ESH";
        break;
    end
end

%%
%Compare previous and next frame type to find the the type
if(prevFrameType=="LSS")
    frameType="ESH";
elseif(prevFrameType=="LPS")
    frameType="OLS";
elseif(prevFrameType=="OLS")
    if(nextFrameType=="ESH")
        frameType="LSS";
    else
        frameType="OLS";
    end
elseif(prevFrameType=="ESH")
    if(nextFrameType=="ESH")
        frameType="ESH";
    else
        frameType="LPS";
    end
end
end
