%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function frameT = iFilterbank(frameF, frameType, winType)
%A function that transforms our samples from frequency domain to time
%domain using the inverse mdct function and then multiplies them with a kaizer bessel
%derived or sinusoid window. Because I use Matlab R2018a, the inverse mdct function
%is not implemented, so I am using the function imdct4 created from Marios
%Athinaiou (http://www.ee.columbia.edu/~marios/).The windows used-created
%for multiplying are the same with the filterbank function
%INPUT
%frameT: 1024x2 matrix with the samples of the two channels in the freq
%domain
%frameType: string containing the type of the current frame
%winType: string containing the type of window to be used, 'KDB' for kaiser
%bessel derived or 'SIN' for sinusoid
%OUTPUT
%frameF: 2048x2 matrix with the samples in the time domain

N=2048;
winFrame=zeros(N,1);
frameT=zeros(N,2);
%%
%OLS
if(frameType=='OLS')
    %Create the window
    if(winType=='KBD')
        w = kaiser(N/2+1,6*pi);
        for n=1:(N/2)
            winFrame(n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        for n=(N/2+1):N
            winFrame(n)=sqrt(sum(w(1:N+1-n))/sum(w));
        end
    elseif(winType=='SIN')
        for n=1:N
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
    end
    
    %Transform the samples to time domain
    frameT=imdct4(frameF);
    %Multiply them with the window
    frameT=frameT.*winFrame;
end    
%%
%LSS
if(frameType=='LSS')
    if(winType=='KBD')
        w = kaiser(N/2+1,6*pi);
        for n=1:(N/2)
            winFrame(n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        winFrame(((N/2)+1):((N/2)+1)+448-1)=ones(448,1);
        w=kaiser(129,6*pi);
        for n=129:256
            l=128-(256-n);
            winFrame(((N/2)+1)+448-1+l)=sqrt(sum(w(1:256+1-n))/sum(w));
        end
        winFrame(((N/2)+1)+448+128:N)=zeros(448,1);
    elseif(winType=='SIN')
        for n=1:(N/2)
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
        winFrame(((N/2)+1):((N/2)+1)+448-1)=ones(448,1);
        for n=129:256
            l=128-(256-n);
            winFrame(((N/2)+1)+448-1+l)=sin((pi/(256))*((n-1)+1/2));
        end
        winFrame(((N/2)+1)+448+128:N)=zeros(448,1);
    end
    
    %Transform the samples to time domain
    frameT=imdct4(frameF);
    %Multiply them with the window
    frameT=frameT.*winFrame;
end    
%%    
%LPS    
if(frameType=='LPS')
    %Create the windows
    if(winType=='KBD')
        winFrame(1:448)=zeros(448,1);
        w=kaiser(129,6*pi);
        for n=1:128
            winFrame(448+n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        winFrame((448+128+1):(448+128+448))=ones(448,1);
        w = kaiser(N/2+1,6*pi);
        for n=(N/2+1):N
            winFrame(n)=sqrt(sum(w(1:N+1-n))/sum(w));
        end
    elseif(winType=='SIN')
        winFrame(1:448)=zeros(448,1);
        for n=1:128
            winFrame(448+n)=sin((pi/(256))*((n-1)+1/2));
        end
        winFrame((448+128+1):(448+128+448))=ones(448,1);
        for n=(N/2+1):N
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
    end
    
    %Transform the samples to time domain
    frameT=imdct4(frameF);
    %Multiply them with the window
    frameT=frameT.*winFrame;
end    
%%
%ESH
if(frameType=='ESH')
    N=256;
    winFrame=zeros(256,1);
    tmpFrame=zeros(2048,2); %as to save the inversed mdct subframes and then process them with the overlapped subframes
    if(winType=='KBD')
        %Create window
        w=kaiser(N/2+1,6*pi);
        for n=1:(N/2)
            winFrame(n)=sqrt(sum(w(1:(n)))/sum(w));
        end
        for n=(N/2+1):N
            winFrame(n)=sqrt(sum(w(1:N+1-n))/sum(w));
        end
     elseif(winType=='SIN')
        for n=1:N
            winFrame(n)=sin((pi/(N))*((n-1)+1/2));
        end
    end
     
    %inverse mdct and multiplication with the window in every subframe
    for i=0:7
        tmpFrame(i*256+1:(i+1)*256,:)=imdct4(frameF(i*128+1:(i+1)*128,:));       
        tmpFrame((i*256+1):((i+1)*256),:)=tmpFrame((i*256+1):((i+1)*256),:).*winFrame;
    end
    
    %first 128 samples are not overlapped
    frameT((448+1):(448+128),:)=tmpFrame(1:128,:);
    k=1;
    %Each subframe is overlapped with its next with 50% of their samples
    %We take the second half of frame n and the first half of frame n+1 to
    %make the overlapping in which we just add the two samples
    for i=1:7
        frameT((448+1+128*i):(448+(i+1)*128),:)=tmpFrame((128*k+1):(128*(k+1)),:)+tmpFrame((128*(k+1)+1):(128*(k+2)),:);
        k=k+2;
    end
    i=i+1;
    frameT((448+1+128*i):(448+(i+1)*128),:)=tmpFrame((128*k+1):2048,:);    
end




end