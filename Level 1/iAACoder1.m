%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function x = iAACoder1(AACSeq1, fNameOut)
%A function that given a file name for a sound file .wav and a struct
%that contains each frame transformed in frequency domain, the type of each
%frame and the window type used, returns the samples in the time domain and
%writes these samples in a wav file with the name given
%INPUT
%fNameOut: string with the name of the file that will be saved
%AACSeq1: struct containing frame samples in freq domain, frame type and
%win type for each frame
%OUTPUT
%x: Nx2, the samples of the two channels in the time domain
%%
%pre-process
nFrames=length(AACSeq1);
tmp=zeros(2048*nFrames,2);
frameF=zeros(1024,2);

%%
%Inverse filterbank
for i=0:nFrames-1
    frameF(:,1)=AACSeq1(i+1).chl.FrameF;
    frameF(:,2)=AACSeq1(i+1).chr.FrameF;
    tmp(1+i*2048:2048+i*2048,:)=iFilterbank(frameF,AACSeq1(i+1).frameType,AACSeq1(i+1).winType);
end

%%
%Final samples
x=zeros(1024*(nFrames+1),2);

x(1:1024,:)=tmp(1:1024,:);
k=1;
%From all the samples each time we take for example (1024:2048)+(2049:3072)
%that means (1*1024:2*1024)+(2*1024+1:3*1024) and then we begin from
%3072=3*1024. That's why we have variable k that increments by 2 for each
%iteration, k is the coefficient before the 1024, and goes like 1-3-5-7 etc
for i=1:nFrames-1
    x(1024+1+(i-1)*1024:1024+i*1024,:)=tmp(1024*k+1:1024*(k+1),:)+tmp(1024*(k+1)+1:1024*(k+2),:);
    k=k+2;
end
i=i+1;
x(1024+1+(i-1)*1024:1024+i*1024,:)=tmp(1024*k+1:1024*(k+1),:);

%%
audiowrite(fNameOut,x,48000);


end