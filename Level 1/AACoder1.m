%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function AACSeq1 = AACoder1(fNameIn)
%A function that given a file name for a sound file .wav, returns a struct
%that contains each frame transformed in frequency domain, the type of each
%frame and the window type used.
%INPUT
%fNameIn: string with the name of the file that we will code
%OUTPUT
%AACSeq1: struct containing frame samples in freq domain, frame type and
%win type for each frame
%%
%pre-processing
[y,Fs] = audioread(fNameIn);
nSamples=length(y(:,1));

%zero padding in the end
if(mod(nSamples,2048)~=0)
    zeroPad=2048-mod(nSamples,2048);
    y(nSamples:nSamples+zeroPad,:)=0;
end

%zero padding in the beggining
nSamples=length(y(:,1));
y(1024+1:nSamples+1024,:)=y;
y(1:1024,:)=0;
nSamples=length(y(:,1));

%%
%SSC
prevFrameType='OLS'; %assuming that the first type is Only Long

%for every sample except the last one
for i=0:(nSamples/1024)-3
    frameT=y(1024*(i)+1:1024*(i+2),:);
    nextFrameT=y(1024*(i+1)+1:1024*(i+3),:);
    frameType = SSC(frameT, nextFrameT, prevFrameType);
    AACSeq1(i+1).frameType=frameType;
    prevFrameType=frameType;
end
i=i+1;
nextFrameT=zeros(2048,2);
frameT=y(1024*(i)+1:1024*(i+2),:);
frameType = SSC(frameT, nextFrameT, prevFrameType);
AACSeq1(i+1).frameType=frameType;
%%
%filterbank
winType='KBD'; %Change from here
for i=0:(nSamples/1024)-2
    frameT=y(1024*(i)+1:1024*(i+2),:);
    frameF=filterbank(frameT, AACSeq1(i+1).frameType, winType);
    AACSeq1(i+1).chl.FrameF=frameF(:,1);
    AACSeq1(i+1).chr.FrameF=frameF(:,2);
    AACSeq1(i+1).winType=winType;
end

end