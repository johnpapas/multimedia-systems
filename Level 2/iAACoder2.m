%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function x = iAACoder2(AACSeq2, fNameOut)
%A function that given a file name for a sound file .wav and a struct
%that contains each frame transformed in frequency domain, the type of each
%frame and the window type used and the TNS coeffs, returns the samples in the time domain and
%writes these samples in a wav file with the name given
%INPUT
%fNameOut: string with the name of the file that will be saved
%AACSeq1: struct containing frame samples in freq domain, frame type,
%win type for each frame and the TNS coeffs for the 2 channels
%OUTPUT
%x: Nx2, the samples of the two channels in the time domain
%%
%pre-process
nFrames=length(AACSeq2);
tmp=zeros(2048*nFrames,2);
frameF=zeros(1024,2);

%%
%Inverse TNS
for i=0:nFrames-1
    frameFLeft=AACSeq2(i+1).chl.FrameF;
    frameFRight=AACSeq2(i+1).chr.FrameF;
    frameF(:,1)=iTNS(frameFLeft, AACSeq2(i+1).frameType, AACSeq2(i+1).chl.TNScoeffs);
    frameF(:,2)=iTNS(frameFRight, AACSeq2(i+1).frameType, AACSeq2(i+1).chr.TNScoeffs);
    tmp(1+i*2048:2048+i*2048,:)=iFilterbank(frameF,AACSeq2(i+1).frameType,AACSeq2(i+1).winType);
end
    

%%
%Inverse filterbank
% for i=0:nFrames-1
%     frameF(:,1)=AACSeq2(i+1).chl.FrameF;
%     frameF(:,2)=AACSeq2(i+1).chr.FrameF;
%     tmp(1+i*2048:2048+i*2048,:)=iFilterbank(frameF,AACSeq2(i+1).frameType,AACSeq2(i+1).winType);
% end

%%
%Final samples
x=zeros(1024*(nFrames+1),2);

x(1:1024,:)=tmp(1:1024,:);
k=1;
for i=1:nFrames-1
    x(1024+1+(i-1)*1024:1024+i*1024,:)=tmp(1024*k+1:1024*(k+1),:)+tmp(1024*(k+1)+1:1024*(k+2),:);
    k=k+2;
end
i=i+1;
x(1024+1+(i-1)*1024:1024+i*1024,:)=tmp(1024*k+1:1024*(k+1),:);

%%
audiowrite(fNameOut,x,48000);


end