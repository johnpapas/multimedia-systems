%Multimedia Systems Project Part 2
%Author Papas Ioannis 9218

function frameFout = iTNS(frameFin, frameType, TNScoeffs)
%A function that illustrates the inverse temporal noise shaping.It applies
%the inverse filter used for the normal TNS, using the same coefficients.
%It returns the initial (if no sound is applied) frequency samples of the
%frame. This function works only for one channel
%INPUT
%frameFin: 1024x1 vector containing the samples in the freq domain
%frameType: String containing the type of the frame
%TNScoeffs: 4x1 for OLS,LSS,LPS or 4x8 for ESH containing the quantized
%coefficients used for the filter.
%OUTPUT
%frameFout: 1024x1 vector containing the filtered samples


if((frameType=="OLS") || (frameType=="LSS") || (frameType=="LPS"))
    %%
    %The coeffs are used for the denominator coeffs
    frameFout=filter(1,[1;-TNScoeffs],frameFin);

elseif(frameType=='ESH')
    %We apply the inverse filter for each subframe and then we reshape the
    %matrix so we return a 1024x1 vector and not 8x128 because we need this
    %as input for the iFilterbank function
    for nFrame=1:8
        frameFout(:,nFrame)=filter(1,[1;-TNScoeffs(:,nFrame)],frameFin(:,nFrame));
    end
    frameFout=reshape(frameFout,[],1);
end

end