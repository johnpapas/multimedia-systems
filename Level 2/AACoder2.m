%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function AACSeq2 = AACoder2(fNameIn)
%A function that given a file name for a sound file .wav, returns a struct
%that contains each frame transformed in frequency domain, the type of each
%frame, the window type used and the TNS coeffs
%INPUT
%fNameIn: string with the name of the file that we will code
%OUTPUT
%AACSeq1: struct containing frame samples in freq domain, frame type,
%win type for each frame and the TNScoeffs
%%
%pre-processing
[y,Fs] = audioread(fNameIn);
nSamples=length(y(:,1));

%zero padding in the end
if(mod(nSamples,2048)~=0)
    zeroPad=2048-mod(nSamples,2048);
    y(nSamples:nSamples+zeroPad,:)=0;
end

%zero padding in the beggining
nSamples=length(y(:,1));
y(1024+1:nSamples+1024,:)=y;
y(1:1024,:)=0;
nSamples=length(y(:,1));

%%
%SSC
prevFrameType='OLS'; %assuming that the first type is Only Long

for i=0:(nSamples/1024)-3
    frameT=y(1024*(i)+1:1024*(i+2),:);
    nextFrameT=y(1024*(i+1)+1:1024*(i+3),:);
    frameType = SSC(frameT, nextFrameT, prevFrameType);
    AACSeq2(i+1).frameType=frameType;
    prevFrameType=frameType;
end
    
i=i+1;
nextFrameT=zeros(2048,2);
frameT=y(1024*(i)+1:1024*(i+2),:);
frameType = SSC(frameT, nextFrameT, prevFrameType);
AACSeq2(i+1).frameType=frameType;
%%
%filterbank
winType='KBD';
for i=0:(nSamples/1024)-2
    frameT=y(1024*(i)+1:1024*(i+2),:);
    frameF=filterbank(frameT, AACSeq2(i+1).frameType, winType);
    AACSeq2(i+1).chl.FrameF=frameF(:,1);
    AACSeq2(i+1).chr.FrameF=frameF(:,2);
    AACSeq2(i+1).winType=winType;
end

%%
%TNS
for i=0:(nSamples/1024)-2
    [AACSeq2(i+1).chl.FrameF,AACSeq2(i+1).chl.TNScoeffs]=TNS(AACSeq2(i+1).chl.FrameF,AACSeq2(i+1).frameType);
    [AACSeq2(i+1).chr.FrameF,AACSeq2(i+1).chr.TNScoeffs]=TNS(AACSeq2(i+1).chr.FrameF,AACSeq2(i+1).frameType);
end



end