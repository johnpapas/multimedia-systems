%Multimedia Systems Project Part 2
%Author Papas Ioannis 9218

function [frameFout, TNScoeffs] = TNS(frameFin, frameType)
%A function that illustrates the temporal noise shaping. It actually finds
%and removes the periodicity in the frequency domain. The periodicity is
%checked when first normalizing our samples based on the Table B219 matrix
%bands. It calculates the tns coefficients which are the coefficients used
%for the final filtering of the samples.This function works only for one channel
%INPUT
%frameFin: 1024x1 vector containing the samples in the freq domain
%frameType: String containing the type of the frame
%OUTPUT
%frameFout: 1024x1 vector containing the filtered samples
%TNScoeffs: 4x1 for OLS,LSS,LPS or 4x8 for ESH containing the quantized
%coefficients used for the filter.

S=load('TableB219.mat');
%%
%This function is the same for OLS, LSS or LPS frame types
if((frameType=="OLS") || (frameType=="LSS") || (frameType=="LPS"))
    %I find the number of bands and their boundaries from the table B219a.
    %I find the energy of each band so i can find the normalization
    %coeffs. Because we dont want steep changes when changing a band, 
    %we smooth this coeffs in the end.
    nSamples=length(frameFin);
    matrix=S.B219a;
    nBands=length(matrix(:,2));
    pj=zeros(nBands,1);
    sw=zeros(nSamples,1);
    matrix(:,2:3)=matrix(:,2:3)+1;
    %Find P(j) and sw(k)
    for j=1:nBands
        pj(j)=sum(frameFin(matrix(j,2):matrix(j,3)).^2);
        sw(matrix(j,2):matrix(j,3))=repmat(sqrt(pj(j)),matrix(j,3)-matrix(j,2)+1,1);
    end
    %Rework sw(k)
    for k=1023:-1:1
        sw(k)=(sw(k)+sw(k+1))/2;
    end
    for k=2:1024
        sw(k)=(sw(k)+sw(k-1))/2;
    end
    %Find xw(k)
    xw=frameFin./sw;
    %%
    %We find the autocorrelation vector for lags from 1 till 4 samples. And
    %then we create the autocorrelation matrix R. In the end we find the
    %TNS coeffs a.
    %Number of coefficients
    p=4;
    %Autocorr vector
    acf=autocorr(xw(:,1), 'NumLags', p);
    r=acf(2:5);
    %Autocorr matrix R
    R=zeros(p);
    %We create a matrix with the indexes we need from the autocorr vector
    %assuming that r(-k)=r(k)
    for i=0:p-1
        A(:,i+1)=(0:3)-i;
    end
    A=abs(A);
    A=A+1;
    for i=1:p
        R(i,:)=acf(A(i,:));
    end
    %Find coefficients
    a=R\r;
    
    %%
    %We quantize the TNS coeffs found using 4 bits and a 0.1 step
    %Quantize coefficients
    quantizerSections=-0.8:0.1:0.8;
    quantizerValues=-0.75:0.1:0.75;
    for i=1:p
        if a(i)<-0.8
            a(i)=-0.75;
            continue;
        elseif a(i)>0.8
            a(i)=0.75;
            continue;
        else
            t=(a(i)>=quantizerSections);
            a(i)=quantizerValues(find(t,1,'last'));
        end
    end
    
    %%
    %We use the coeffs after quantizing them so we can filter our input
    %samples. The coeffs are used for the numerator
    %Filtering
    frameFout=filter([1;-a],1,frameFin);
    if (abs(roots([1;-a]))>=1)
        tmp=[1;-a];
        tmp(abs(roots([1;-a]))>=1)=1;
        tmp=poly(tmp);
        a=tmp(2:5)/tmp(1);
    end
    TNScoeffs=a;
    
elseif(frameType=='ESH')
    %We use the same procedure as previously. The only differences is that
    %we have different bands because the function is used for each
    %subframe
    
    matrix=S.B219b;
    frameFout=zeros(128,8);
    TNScoeffs=zeros(4,8);
    nBands=length(matrix(:,2));
    pj=zeros(nBands,1);
    sw=zeros(128,1);
    matrix(:,2:3)=matrix(:,2:3)+1;
    for nFrame=0:7
        frame=frameFin(1+128*nFrame:128*(nFrame+1));
        for j=1:nBands
            pj(j)=sum(frame(matrix(j,2):matrix(j,3)).^2);
            sw(matrix(j,2):matrix(j,3))=repmat(sqrt(pj(j)),matrix(j,3)-matrix(j,2)+1,1);
        end
        for k=127:-1:1
            sw(k)=(sw(k)+sw(k+1))/2;
        end
        for k=2:128
            sw(k)=(sw(k)+sw(k-1))/2;
        end
        xw=frame./sw;
        p=4;
        acf=autocorr(xw(:,1), 'NumLags', p);
        r=acf(2:5);
        R=zeros(p);
        for i=0:p-1
            A(:,i+1)=(0:3)-i;
        end
        A=abs(A);
        A=A+1;
        for i=1:p
            R(i,:)=acf(A(i,:));
        end
        a=R\r;
        quantizerSections=-0.8:0.1:0.8;
        quantizerValues=-0.75:0.1:0.75;
        for i=1:p
            if a(i)<-0.8
                a(i)=-0.75;
                continue;
            elseif a(i)>0.8
                a(i)=0.75;
                continue;
            else
                t=(a(i)>=quantizerSections);
                a(i)=quantizerValues(find(t,1,'last'));
            end
        end      
        frameFout(:,nFrame+1)=filter([1;-a],1,frame);
        TNScoeffs(:,nFrame+1)=a;
    end
end
