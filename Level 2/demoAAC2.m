%Multimedia Systems Project Part 1
%Author Papas Ioannis 9218

function SNR = demoAAC2(fNameIn, fNameOut)
%%
%Get the wav file 
[y,~] = audioread(fNameIn);
nSamples=length(y(:,1));
%zero padding
if(mod(nSamples,2048)~=0)
    zeroPad=2048-mod(nSamples,2048);
    y(nSamples:nSamples+zeroPad,:)=0;
end
nSamples=length(y(:,1));
y(1024+1:nSamples+1024,:)=y;
y(1:1024,:)=0;

%%
%Coding-Decoding
AACSeq2 = AACoder2(fNameIn);

x = iAACoder2(AACSeq2, fNameOut);

SNR=snr(y,abs(y-x));

fprintf("The SNR is %d", SNR);

end